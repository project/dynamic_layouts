# Dynamic Layouts

This Drupal 8 "Layout Builder" module can be used to create/manage reuseable
layouts, which can be used in Display Suite, Panels & the Core Layout Builder.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/dynamic_layouts)

To submit bug reports and feature suggestions, or to track changes
[issue queue](https://www.drupal.org/project/issues/dynamic_layouts)


## Requirements

- Drupal core version: >= 8.3.0
- A module that consumes layouts:
    - Display Suite
    - Panels
    - Core Layout Builder
    - Or any other project that consumes layouts.


## Installation

Install as usual, see [Installing Drupal Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.


## Configuration

- Configure user permissions in `Administration » People » Permissions`:

    - Administer Dynamic Layouts

      General permission for managing the Dynamic Layouts.

- Customize the Dynamic Layout settings in
  `Structure » Dynamic Layouts » Settings`


## Maintainers

- Falco Bahnerth - [falco010](https://www.drupal.org/u/falco010)
- Fabian de Rijk - [fabianderijk](https://www.drupal.org/u/fabianderijk)
- Pravin Gaikwad - [Rajeshreeputra](https://www.drupal.org/u/rajeshreeputra)

**This project has been sponsored by:**

- Youwe - As small as it can be and as big as it must be. We are organised
  in self-regulated teams: circles. you profit from the knowledge of 110
  specialists, whilst having direct contact with one circle. together with
  major customers, we book great results. And we have been doing so since 2000.
  Full service, from strategy and concept to realization and conversion.
  Visit [Youwe](https://www.youwe.nl) for more information
- Finalist - Sponsoring and support
- Shivswarajya Inc. - Sponsoring and support
